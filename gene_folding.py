import sys
def gene_validation(gene):
    flag = True
    for i in range(len(gene)):
        if gene[i].upper() not in ['A', 'C', 'G', 'T']:
            flag = False
            break
    return flag
def goof(gene):
    if gene_validation(gene):
        gene_folding = gene
        seq_len = len(gene) // 2
        while seq_len != -1:
            if gene[:seq_len][::-1] == gene[seq_len:seq_len + seq_len]:
                gene = gene[seq_len:]
                seq_len = -1
            else:
                seq_len -= 1
        gene = gene[::-1]

        seq_len = len(gene) // 2
        while seq_len != -1:
            if gene[:seq_len][::-1] == gene[seq_len:seq_len + seq_len]:
                gene = gene[seq_len:]
                seq_len = -1
            else:
                seq_len -= 1
        gene = gene[::-1]

        if gene == gene_folding:
            return len(gene)
        return goof(gene)

gene = sys.argv[1]
print(goof(gene))
